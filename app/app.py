import os
from bottle import route, run, template

@route('/')
def hello():
    return template('counter', first_name=os.environ['first_name'])

run(host='0.0.0.0', port=5000, debug=True)