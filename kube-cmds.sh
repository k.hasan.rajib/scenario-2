# create DB secrets
kubectl create -f app-secrets.yml
# activate minikube docker-env
eval $(minikube -p minikube docker-env)
# build docker image
cd app && docker build . -t counter-app && cd ..
# minikube image load counter-app
# create app deployment
kubectl create -f app-deployment.yml
# create app service
kubectl create -f app-service.yml

## check the deployed service
minikube service --url counter-service

# download mysql:8 and save to minikube
docker pull mysql:8
minikube image load mysql:8
# create mysql service
kubectl create -f mysql-deployment.yml

# test mysql connection from web tier
kubectl exec -it service/counter-service -- mysql --host=counter-mysql --user=root --password=g5r9OBWH17hF --execute='show databases;'

# install prometheus-community version
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np
# access prometheus
minikube service prometheus-server-np

# install grafana
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
# Collect 'admin' user password for grafana
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode
# Expose Grafana
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-np
## access grafana
minikube service grafana-np
