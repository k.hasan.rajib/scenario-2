# scenario-2

## Components

- Kubernetes on Minikube
- Docker as driver for minikube
- Helm charts for Grafana and prometheus

## Deliverables:
- Displaying *first_name*
  - `first_name` env var is added to pod env at https://gitlab.com/k.hasan.rajib/scenario-2/-/blob/master/app-deployment.yml#L32
  - `first_name` grabed from env and passed to front-end https://gitlab.com/k.hasan.rajib/scenario-2/-/blob/master/app/app.py#L6
  - Final ingress with first name:
  <br><hr><img src="screenshots/ingress.png"  width="320" height="240">
- Connecting to MySQL from web app instance:
  - MySQL password created as secret at https://gitlab.com/k.hasan.rajib/scenario-2/-/blob/master/app-secrets.yml#L7 and is set at https://gitlab.com/k.hasan.rajib/scenario-2/-/blob/master/mysql-deployment.yml#L51
  - Connection to mysql from web tier to mysql db can be tested using command `kubectl exec -it service/counter-service -- mysql --host=counter-mysql --user=root --password=g5r9OBWH17hF --execute='show databases;'`. Details is in Setup steps
- Prometheus and Grafana
  - Check Setup Steps for installation steps
  - Connecting prometheus, importing Kubernetes Dashboard, :
    <br><hr><img src="screenshots/connecting-prometheus.png"  width="320" height="240"><img src="screenshots/importing-dashboard.png"  width="320" height="240"><br><img src="screenshots/grafana-dashboard.png"  width="320" height="240">

## Setup steps
```bash
# create DB secrets
kubectl create -f app-secrets.yml
# activate minikube docker-env
eval $(minikube -p minikube docker-env)
# build docker image
cd app && docker build . -t counter-app && cd ..
# minikube image load counter-app
# create app deployment
kubectl create -f app-deployment.yml
# create app service
kubectl create -f app-service.yml

## check the deployed service
minikube service --url counter-service

# download mysql:8 and save to minikube
docker pull mysql:8
minikube image load mysql:8
# create mysql service
kubectl create -f mysql-deployment.yml

# test mysql connection from web tier
kubectl exec -it service/counter-service -- mysql --host=counter-mysql --user=root --password=g5r9OBWH17hF --execute='show databases;'

# install prometheus-community version
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install prometheus prometheus-community/prometheus
kubectl expose service prometheus-server --type=NodePort --target-port=9090 --name=prometheus-server-np
# access prometheus
minikube service prometheus-server-np

# install grafana
helm repo add grafana https://grafana.github.io/helm-charts
helm install grafana grafana/grafana
# Collect 'admin' user password for grafana
kubectl get secret --namespace default grafana -o jsonpath="{.data.admin-password}" | base64 --decode
# Expose Grafana
kubectl expose service grafana --type=NodePort --target-port=3000 --name=grafana-np
## access grafana
minikube service grafana-np
```
## Troubleshooting tips:
- Use localhost on Mac apple chip while working with minikube for ingress. Relevant discussion: https://github.com/kubernetes/minikube/issues/7332
- Docker MySQL 5.6-7 are not supported on mac arm chip. User mysql 8 docker image
- Minikube kubectl autocompletion activation is messy. Some hints https://minikube.sigs.k8s.io/docs/commands/completion/
- Consider using Hyperkit or virtualbox as driver for Minikube instead of Docker. Driver option: https://minikube.sigs.k8s.io/docs/drivers/
